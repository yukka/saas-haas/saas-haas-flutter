import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:saashaas/app/Services/utils/string_helper.dart';

Map<String, dynamic> SOCIAL_PROVIDERS = <String, dynamic>{
  'google': <String, dynamic>{
    'enabled': dotenv.env["GOOGLE_SIGN_IN_ENABLED"]!.toBool(),
    'label': "Google",
    'color': Colors.red,
    'icon': "assets/images/social_media_icons/google.svg",
  },
  'facebook': <String, dynamic>{
    'enabled': dotenv.env["FACEBOOK_SIGN_IN_ENABLED"]!.toBool(),
    'label': "Facebook",
    'color': Colors.blueAccent,
    'icon': "assets/images/social_media_icons/facebook.svg",
  },
  'apple': <String, dynamic>{
    'enabled': dotenv.env["APPLE_SIGN_IN_ENABLED"]!.toBool(),
    'label': "Apple",
    'color': Colors.blueGrey,
    'icon': "assets/images/social_media_icons/apple.svg",
  },
};
