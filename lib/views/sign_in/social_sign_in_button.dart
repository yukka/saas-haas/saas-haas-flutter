import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:saashaas/app/Providers/providers.dart';
import 'package:easy_localization/easy_localization.dart';

class SocialSignInButton extends ConsumerWidget {
  final Map<String, dynamic> socialProvider;
  final bool fullLabelText;

  SocialSignInButton(this.socialProvider, {this.fullLabelText = false});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _auth = ref.watch(authProvider);

    return Flexible(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.only(left: 5, right: 5),
          child: ElevatedButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
              overlayColor: MaterialStateProperty.all<Color>(
                  socialProvider['color'].withOpacity(0.1)),
            ),
            onPressed: () async {
              _auth.signInWithSocialMedia(socialProvider['label']);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  socialProvider['icon'],
                  color: socialProvider['color'],
                  width: 16,
                  height: 16,
                ),
                SizedBox(width: 8),
                fullLabelText
                    ? Text("Sign in with {}")
                        .tr(args: [socialProvider['label']])
                    : Text(socialProvider['label']),
                // ,
              ],
            ),
          ),
        ));
  }
}
