import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:saashaas/app/Providers/providers.dart';
import 'package:saashaas/app/Services/authentication/authentication_service.dart';
import 'package:saashaas/config/social_providers.dart';
import 'package:saashaas/views/components/app/splash.dart';
import 'package:saashaas/views/home.dart';
import 'package:saashaas/views/sign_in/components/loading_popup.dart';
import 'package:saashaas/views/sign_in/social_sign_in_button.dart';
import 'package:saashaas/app/Services/utils/string_helper.dart';

import 'email_login_form.dart';

class SignInLanding extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _auth = ref.watch(authProvider);

    return ProviderListener<AuthenticationService>(
        onChange: (context, auth) {
          if (auth.user != null) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Home()),
            );
          }
        },
        provider: authProvider,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Scaffold(
            body: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 50, right: 50),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      // Logo
                      Padding(
                        padding: EdgeInsets.only(bottom: 60),
                        child: SvgPicture.asset('assets/images/gust/logo.svg',
                            height: 100),
                      ),
                      // Email login form
                      if (dotenv.env['EMAIL_SIGN_IN_ENABLED']!.toBool())
                        EmailLoginForm(),

                      // Social login form (vertical)
                      if (dotenv.env['EMAIL_SIGN_IN_ENABLED']!.toBool() ==
                          false)
                        for (Map<String, dynamic> providerDetails
                            in SOCIAL_PROVIDERS.values
                                .where((provider) => provider['enabled']))
                          SocialSignInButton(
                            providerDetails,
                            fullLabelText: true,
                          ),

                      // Social login form (horizontal)
                      if (dotenv.env['EMAIL_SIGN_IN_ENABLED']!.toBool() &&
                          dotenv.env['SOCIAL_LOGIN_ENABLED']!.toBool())
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            for (Map<String, dynamic> providerDetails
                                in SOCIAL_PROVIDERS.values
                                    .where((provider) => provider['enabled']))
                              SocialSignInButton(providerDetails)
                          ],
                        ),

                      // Show errors
                      Visibility(
                        visible: _auth.authError != null,
                        child: Container(
                          padding: EdgeInsets.only(top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                _auth.authError ?? "",
                                style:
                                    TextStyle(color: Colors.red, fontSize: 12),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                // Loading dialog
                Visibility(
                  visible: _auth.isAuthenticating,
                  child: LoadingPopup(),
                ),

                // Show splash screen during auto login
                Visibility(visible: !_auth.autoLoginProcessed, child: Splash()),
              ],
            ),
          ),
        ));
  }
}
