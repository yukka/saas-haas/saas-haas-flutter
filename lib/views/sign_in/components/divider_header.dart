import 'package:flutter/material.dart';

class DividerHeader extends StatelessWidget {

  final String title;

  const DividerHeader({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(children: <Widget>[
        Expanded(
          child: new Container(
              margin: const EdgeInsets.only(left: 10.0, right: 20.0),
              child: Divider(
                thickness: 0.6,
                color: Colors.grey,
                height: 36,
              )),
        ),
        Text(
          this.title,
          style: TextStyle(
            fontSize: 14,
            color: Colors.grey,
          ),
        ),
        Expanded(
          child: new Container(
              margin: const EdgeInsets.only(left: 20.0, right: 10.0),
              child: Divider(
                thickness: 0.6,
                color: Colors.grey,
                height: 36,
              )),
        ),
      ]),
    );
  }
}
