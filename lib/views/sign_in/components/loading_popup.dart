import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class LoadingPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colors.black.withOpacity(0.5),
        ),
        AlertDialog(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 16,
                width: 16,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              ),
              SizedBox(width: 16),
              Text('Signing in...'.tr(), style: TextStyle(fontSize: 14)),
            ],
          ),
        ),
      ],
    );
  }
}
