import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:saashaas/app/Services/utils/string_helper.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:saashaas/app/Providers/providers.dart';

import 'components/divider_header.dart';


class EmailLoginForm extends ConsumerStatefulWidget {
  const EmailLoginForm({Key? key}) : super(key: key);

  @override
  _EmailLoginFormState createState() => _EmailLoginFormState();
}

class _EmailLoginFormState extends ConsumerState<EmailLoginForm> {

  final emailTextController = TextEditingController();

  final passwordTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    final node = FocusScope.of(context);

    final authProviderWatcher = ref.watch(authProvider);

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: TextField(
            controller: emailTextController,
            style: TextStyle(
              fontSize: 14.0,
            ),
            onEditingComplete: () => node.nextFocus(),
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Email',
              hintText: 'Enter your email address',
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: TextField(
            controller: passwordTextController,
            obscureText: true,
            onEditingComplete: () => node.unfocus(),
            style: TextStyle(
              fontSize: 14.0,
            ),
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
                hintText: 'Enter your secure password'),
          ),
        ),
        ElevatedButton(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.teal),
          ),
          onPressed: () async {
            authProviderWatcher.signInWithEmail(
                emailTextController.text,
                passwordTextController.text
            );
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Login")],
          ),
        ),
        if (dotenv.env['REGISTER_FORM_ENABLED']!.toBool())
          TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.teal),
            ),
            onPressed: () async {},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("No account? Click here to register",
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400))
              ],
            ),
          ),
        if (dotenv.env['SOCIAL_LOGIN_ENABLED']!.toBool())
          DividerHeader(title: 'Or use social media'),
      ],
    );
  }
}
