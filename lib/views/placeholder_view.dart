import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PlaceholderView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset('assets/images/gust/placeholder.svg', height: 200),
        SizedBox(height: 28),
        Text("Let's create something beautiful.",
            style: TextStyle(color: Colors.grey, fontSize: 16)),
      ],
    ));
  }
}
