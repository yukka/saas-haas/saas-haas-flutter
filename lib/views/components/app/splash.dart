import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Color(0xff403d56),
      child: Center(
        child: Icon(Icons.airplanemode_on_rounded,
            size: MediaQuery.of(context).size.width * 0.5, color: Colors.white),
      ),
    );
  }
}
