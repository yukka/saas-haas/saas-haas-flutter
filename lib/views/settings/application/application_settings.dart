import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:saashaas/app/Services/utils/toast_service.dart';
import 'package:saashaas/config/localization.dart';
import 'package:saashaas/views/settings/components/setting_switch_tile.dart';
import 'package:saashaas/views/settings/components/setting_tile.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:saashaas/views/settings/components/settings_section_header.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked_themes/stacked_themes.dart';
import 'package:saashaas/app/Services/utils/string_helper.dart';

class ApplicationSettings extends StatefulWidget {
  const ApplicationSettings({Key? key}) : super(key: key);

  @override
  _ApplicationSettingsState createState() => _ApplicationSettingsState();
}

class _ApplicationSettingsState extends State<ApplicationSettings> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SettingsSectionHeader(title: "Application"),
        ListView(
          padding: EdgeInsets.only(top: 0.0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: ListTile.divideTiles(context: context, tiles: [
            if (dotenv.env["SUPPORT_DARK_THEME"]!.toBool())
              SettingSwitchTile(
                title: 'Dark theme'.tr(),
                switchValue: getThemeManager(context).isDarkMode,
                leading: Icon(Icons.format_paint_outlined),
                onToggle: (bool newValue) async {
                  getThemeManager(context).toggleDarkLightTheme();
                  final prefs = await SharedPreferences.getInstance();
                  prefs.setBool(
                      "dark_mode_enabled", getThemeManager(context).isDarkMode);
                  setState(() {});
                },
              ),
            if (LANGUAGES.length > 1)
              SettingTile(
                title: 'Language'.tr(),
                subtitle: (context.locale).toString().tr(),
                leading: Icon(Icons.language),
                onPressed: (BuildContext context) async {
                  await _languagePickerAlertDialog(context);
                  setState(() {});
                },
              ),
            SettingTile(
              title: 'Notifications'.tr(),
              subtitle: 'All'.tr(),
              leading: Icon(Icons.notifications_active_outlined),
              onPressed: (BuildContext context) {
                print("hha");
              },
            ),
          ]).toList(),
        ),
      ],
    );
  }
}

Future<void> _languagePickerAlertDialog(BuildContext context) async {
  double iconSize = 20;
  double iconPaddingRight = 8;

  return await showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return SimpleDialog(
          title: const Text('Select Language'),
          children: [
            for (var languageCode in LANGUAGES)
              SimpleDialogOption(
                onPressed: () {
                  context.setLocale(Locale(languageCode));
                  ToastService.showSuccessToast(
                      "Successfully updated language.");
                  Navigator.pop(context);
                },
                child: Row(
                  children: [
                    Visibility(
                      visible: context.locale == Locale(languageCode),
                      child: Padding(
                          padding: EdgeInsets.only(right: iconPaddingRight),
                          child: Icon(
                            Icons.check,
                            size: iconSize,
                          )),
                    ),
                    Visibility(
                      visible: context.locale != Locale(languageCode),
                      child: Padding(
                          padding: EdgeInsets.only(right: iconPaddingRight),
                          child: SizedBox(
                            width: iconSize,
                          )),
                    ),
                    Text(languageCode.tr())
                  ],
                ),
              ),
          ],
        );
      });
}
