import 'package:flutter/material.dart';
import 'package:saashaas/views/settings/components/setting_tile.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:saashaas/app/Providers/providers.dart';
import 'package:saashaas/views/settings/components/settings_section_header.dart';
import 'package:saashaas/views/settings/devices/devices_list.dart';
import 'package:saashaas/views/sign_in/sign_in_landing.dart';

class SessionSettings extends ConsumerStatefulWidget {
  const SessionSettings({Key? key}) : super(key: key);

  @override
  _SessionSettingsState createState() => _SessionSettingsState();
}

class _SessionSettingsState extends ConsumerState<SessionSettings> {

  @override
  Widget build(BuildContext context) {
    final _auth = ref.read(authProvider);

    return Column(
      children: [
        SettingsSectionHeader(title: "Account"),
        ListView(
          padding: EdgeInsets.only(top: 0.0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: ListTile.divideTiles(context: context, tiles: [
            SettingTile(
              title: 'Devices'.tr(),
              subtitle: '5 sessions',
              leading: Icon(Icons.devices),
              onPressed: (BuildContext context) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DevicesList()),
                );
              },
            ),
            SettingTile(
              title: 'Delete Account'.tr(),
              leading: Icon(Icons.delete_forever_outlined),
              onPressed: (BuildContext context) {
                print("hha");
              },
            ),
            SettingTile(
              title: 'Sign Out'.tr(),
              leading: Icon(Icons.logout),
              onPressed: (BuildContext context) async {
                await _auth.logout();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => SignInLanding()));
              },
            ),
          ]).toList(),
        ),
      ],
    );
  }
}
