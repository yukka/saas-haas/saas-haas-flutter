import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:saashaas/app/Providers/providers.dart';
import 'package:saashaas/app/models/Device.dart';

class DevicesList extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _auth = ref.watch(authProvider);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
            color: Colors.grey
        ),
        title: Text('Devices'.tr(), style: TextStyle(
            color: Colors.grey
        )),
      ),
      body: ListView.builder(
          itemCount: _auth.user!.devices.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              dense: true,
                contentPadding:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                title: Text(_auth.user!.devices[index].name,
                    style: TextStyle(fontWeight: FontWeight.bold)),
                subtitle: Text("85.174.35.159 - Groningen, Netherlands"),
                trailing: Text("Beëindig", style: TextStyle(color: Colors.red)),
                onTap: () => _endDeviceSessionAlertDialog(context, _auth.user!.devices[index], _auth));
          }),
    );
  }

  Future<void> _endDeviceSessionAlertDialog(BuildContext context, Device device, auth) async {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(device.name),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Wil je deze sessie echt beëindigen?'),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Annuleren', style: TextStyle(color: Colors.black)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Beëindigen', style: TextStyle(color: Colors.red)),
              onPressed: () {
                auth.removeToken(device);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
