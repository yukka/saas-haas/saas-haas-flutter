import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:saashaas/views/settings/sessions/session_settings.dart';
import 'package:saashaas/app/Services/utils/string_helper.dart';
import 'account/account_settings.dart';
import 'application/application_settings.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          SizedBox(height: 20),
          if (dotenv.env["SIGN_IN_ENABLED"]!.toBool()) AccountSettings(),
          ApplicationSettings(),
          if (dotenv.env["SIGN_IN_ENABLED"]!.toBool()) SessionSettings(),
        ],
      ),
    );
  }
}
