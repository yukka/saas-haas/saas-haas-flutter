import 'package:flutter/material.dart';

class SettingTile extends StatelessWidget {
  final String title;
  final String? subtitle;
  final Widget? leading;
  final Widget? trailing;
  final Function(BuildContext context)? onPressed;

  const SettingTile(
      {Key? key,
      required this.title,
      this.subtitle,
      this.leading,
      this.trailing,
      this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        height: double.infinity,
        width: 30,
        child: leading,
      ),
      title: Text(title),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (subtitle != null)
            Text(subtitle!, style: TextStyle(color: Colors.grey[500])),
          Icon(
            Icons.chevron_right,
            color: Colors.grey[500],
          ),
        ],
      ),
      onTap: onTapFunction(context) as void Function()?,
    );
  }

  Function? onTapFunction(BuildContext context) => onPressed != null
      ? () {
          if (onPressed != null) {
            onPressed!.call(context);
          }
        }
      : null;
}
