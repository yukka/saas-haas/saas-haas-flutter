import 'package:flutter/material.dart';

class SettingSwitchTile extends StatefulWidget {
  final String title;
  final String? subtitle;
  final Widget? leading;
  final Widget? trailing;
  final bool switchValue;
  final Function(bool value)? onToggle;

  const SettingSwitchTile(
      {Key? key,
      required this.title,
      this.subtitle,
      this.leading,
      this.trailing,
      required this.switchValue,
      required this.onToggle})
      : super(key: key);

  @override
  _SettingSwitchTileState createState() => _SettingSwitchTileState();
}

class _SettingSwitchTileState extends State<SettingSwitchTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Container(
        height: double.infinity,
        width: 30,
        child: widget.leading,
      ),
      title: Text(widget.title),
      onTap: () {
        widget.onToggle!(!widget.switchValue);
      },
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Switch(
            value: widget.switchValue,
            onChanged: widget.onToggle,
          ),
        ],
      ),
    );
  }
}
