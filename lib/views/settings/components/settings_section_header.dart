import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class SettingsSectionHeader extends StatelessWidget {
  final String title;

  const SettingsSectionHeader({Key? key, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 15, 20, 10),
      child: Row(children: [
        Text(title, style: TextStyle(fontWeight: FontWeight.w600)).tr(),
      ]),
    );
  }
}
