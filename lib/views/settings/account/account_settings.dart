import 'package:flutter/material.dart';
import 'package:saashaas/views/settings/components/setting_tile.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:saashaas/app/Providers/providers.dart';
import 'package:saashaas/views/settings/components/settings_section_header.dart';

class AccountSettings extends ConsumerStatefulWidget {
  const AccountSettings({Key? key}) : super(key: key);

  @override
  _AccountSettingsState createState() => _AccountSettingsState();
}

class _AccountSettingsState extends ConsumerState<AccountSettings> {
  @override
  Widget build(BuildContext context) {
    final _auth = ref.read(authProvider);

    return Column(
      children: [
        SettingsSectionHeader(title: "Account"),
        ListView(
          padding: EdgeInsets.only(top: 0.0),
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: ListTile.divideTiles(context: context, tiles: [
            SettingTile(
              title: 'Name'.tr(),
              subtitle: _auth.user != null ? _auth.user!.name : '',
              leading: Icon(Icons.person_outline),
              onPressed: (BuildContext context) {
                print("hha");
              },
            ),
            SettingTile(
              title: 'Email'.tr(),
              subtitle: _auth.user != null ? _auth.user!.email : '',
              leading: Icon(Icons.mail_outline, size: 20),
              onPressed: (BuildContext context) {
                print("hha");
              },
            ),
          ]).toList(),
        ),
      ],
    );
  }
}
