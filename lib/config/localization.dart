import 'package:flutter_dotenv/flutter_dotenv.dart';

String languagesString = dotenv.env["LANGUAGES"]!.trim().toLowerCase();
List<String> LANGUAGES = languagesString.split(",");