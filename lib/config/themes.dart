import 'package:flutter/material.dart';

/// Define light theme colours
// ignore: non_constant_identifier_names
ThemeData LIGHT_THEME = ThemeData(
  brightness: Brightness.light,
  primarySwatch: Colors.teal,
);

/// Define dark theme colours
// ignore: non_constant_identifier_names
ThemeData DARK_THEME = ThemeData(
  brightness: Brightness.dark,
  canvasColor: Color.fromRGBO(26, 39, 48, 1),
  scaffoldBackgroundColor: Color.fromRGBO(26, 39, 48, 1),
  primarySwatch: Colors.teal,
);
