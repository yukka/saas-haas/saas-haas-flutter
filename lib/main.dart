import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:saashaas/config/themes.dart';
import 'package:saashaas/views/home.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stacked_themes/stacked_themes.dart';
import 'config/localization.dart';
import 'views/sign_in/sign_in_landing.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:saashaas/app/Services/utils/string_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await dotenv.load(fileName: ".env");
  await ThemeManager.initialise();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  ThemeMode defaultTheme = await dotenv.env["SUPPORT_DARK_THEME"]!.toBool() &&
          (prefs.getBool("dark_mode_enabled") != null
              ? prefs.getBool("dark_mode_enabled")!
              : false)
      ? ThemeMode.dark
      : ThemeMode.light;

  runApp(ProviderScope(
    child: EasyLocalization(
      supportedLocales: [
        for (var languageCode in LANGUAGES) Locale(languageCode),
      ],
      path: dotenv.env["LANGUAGE_PATH"] ?? 'assets/lang',
      fallbackLocale: Locale(dotenv.env["FALLBACK_LANGUAGE"]!),
      child: MyApp(defaultTheme: defaultTheme),
    ),
  ));
}

class MyApp extends StatelessWidget {
  final ThemeMode defaultTheme;

  const MyApp({Key? key, required this.defaultTheme}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ThemeBuilder(
      defaultThemeMode: defaultTheme,
      darkTheme:
          dotenv.env["SUPPORT_DARK_THEME"]!.toBool() ? DARK_THEME : LIGHT_THEME,
      lightTheme: LIGHT_THEME,
      builder: (context, regularTheme, darkTheme, themeMode) => MaterialApp(
        title: dotenv.env["APP_NAME"]!,
        theme: regularTheme,
        darkTheme: darkTheme,
        themeMode: themeMode,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        home:
            dotenv.env["SIGN_IN_ENABLED"]!.toBool() ? SignInLanding() : Home(),
      ),
    );
  }
}
