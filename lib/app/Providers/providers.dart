import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:saashaas/app/Services/authentication/authentication_service.dart';

final authProvider = ChangeNotifierProvider<AuthenticationService>((ref) {
  return AuthenticationService.instance();
});