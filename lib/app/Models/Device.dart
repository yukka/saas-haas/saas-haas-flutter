class Device {
  final int id;
  final String name;

  Device(this.id, this.name);

  Device.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'email': name,
      };
}
