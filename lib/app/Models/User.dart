import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:saashaas/app/Services/api/api_helper.dart';
import 'dart:convert';

import 'Device.dart';

class User {
  final String name;
  final String email;
  final String avatar;
  List<Device> devices = [];

  User(this.name, this.email, this.avatar);

  User.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        email = json['email'],
        avatar = json['avatar'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'email': email,
        'avatar': avatar,
      };

  Future<List<Device>> getDevices() async {
    List<Device> devices = [];
    Response response = await ApiHelper.get('/api/user/devices');
    if (response.statusCode == 200) {
      for (var deviceJson in jsonDecode(response.body)) {
        devices.add(Device.fromJson(deviceJson));
      }
      this.devices = devices;
    } else {
      throw Exception('Failed to retrieve devices');
    }
    return devices;
  }

  removeDevice(Device device) async {
    List<Device> devices = [];
    Response response = await ApiHelper.delete('/api/user/device/${device.id}');
    if (response.statusCode == 200) {
      for (var deviceJson in jsonDecode(response.body)) {
        devices.add(Device.fromJson(deviceJson));
      }
      this.devices = devices;
    } else {
      throw Exception('Failed to delete device');
    }
  }
}
