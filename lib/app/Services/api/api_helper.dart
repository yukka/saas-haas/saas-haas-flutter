import 'dart:core';
import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class ApiHelper {
  static const apiScheme = 'https';
  static const apiHost = 'saashaas.yukka.io';
  static const prefix = '/api/';

  /// Get user token from secure storage
  static Future getApiToken() async {
    String? token = await new FlutterSecureStorage().read(key: 'token');
    return token ?? "";
  }

  /// GET request
  static Future<Response> get(String endpoint,
      [Map<String, dynamic>? queryParameters]) async {
    Uri uri = new Uri.https(
        '${dotenv.env["API_BASE_URL"]}', endpoint, queryParameters);
    Response response = await http.get(uri, headers: {
      HttpHeaders.authorizationHeader: "Bearer ${await getApiToken()}",
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    return response;
  }

  /// POST request
  static Future<Response> post(String endpoint,
      [Map<String, dynamic>? queryParameters]) async {
    Uri uri = new Uri.https('${dotenv.env["API_BASE_URL"]}', endpoint);
    Response response = await http.post(uri, body: queryParameters, headers: {
      HttpHeaders.authorizationHeader: "Bearer ${await getApiToken()}",
    });
    return response;
  }

  /// DELETE request
  static Future<Response> delete(String endpoint,
      [Map<String, dynamic>? queryParameters]) async {
    Uri uri = new Uri.https('${dotenv.env["API_BASE_URL"]}', endpoint);
    Response response = await http.delete(uri, body: queryParameters, headers: {
      HttpHeaders.authorizationHeader: "Bearer ${await getApiToken()}",
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    });
    return response;
  }
}
