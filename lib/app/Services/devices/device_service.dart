import 'dart:io';

import 'package:device_info/device_info.dart';

class DeviceService {
// Get the device name, e.g. 'Sony Xperia II'
  static Future<String> getDeviceName() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        return (await deviceInfo.androidInfo).model;
      } else if (Platform.isIOS) {
        return (await deviceInfo.iosInfo).name;
      }
    } catch (error) {
      print(error);
    }
    return 'Invalid device';
  }
}
