extension BoolParsing on String {
  bool toBool() {
    return this.toLowerCase() == 'true' || this == '1';
  }
}
