import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:saashaas/app/Services/api/api_helper.dart';
import 'package:saashaas/app/Services/devices/device_service.dart';
import 'package:saashaas/app/Services/utils/toast_service.dart';
import 'package:saashaas/app/models/Device.dart';
import 'package:saashaas/app/models/User.dart';
import 'social_providers/google_auth_service.dart';

class AuthenticationService extends ChangeNotifier {
  AuthenticationService.instance() {
    signInWithToken();
  }

  User? get user => _user;

  String? accessToken;
  User? _user;
  bool isAuthenticating = false;
  bool autoLoginProcessed = false;
  String? authError;

  /// Try to sign if there's a user token saved in the storage
  Future<void> signInWithToken() async {
    if (await new FlutterSecureStorage().containsKey(key: 'token')) {
      await retrieveUser();
    }
    await Future.delayed(Duration(milliseconds: 500));
    autoLoginProcessed = true;
    notifyListeners();
  }

  /// Try to sign if there's an access token set
  Future<void> _signInWithAccessToken(String socialProvider) async {
    startAuthentication();

    // Get a user token...
    Response response = await ApiHelper.post('/api/auth/login/social', {
      'provider': socialProvider,
      'token': accessToken,
      'device_name': await DeviceService.getDeviceName()
    });

    if (response.statusCode == 200 || response.statusCode == 201) {
      String token = jsonDecode(response.body)['token'];
      new FlutterSecureStorage().write(key: 'token', value: token);

      // Retrieve the user...
      await retrieveUser();
    } else {
      handleError('Failed to retrieve token.');
    }
  }

  /// Start authentication
  void startAuthentication() {
    authError = null;
    isAuthenticating = true;
    notifyListeners();
  }

  /// Signing in has been finished
  void finishedAuthentication() {
    isAuthenticating = false;
    notifyListeners();
  }

  /// Handle auth errors
  Future<void> handleError(String message, [bool showToUser = true]) async {
    finishedAuthentication();
    ToastService.showDangerToast(message);
    if(showToUser) {
      authError = message;
      notifyListeners();
    }
  }

  /// Signing in has been finished
  Future<void> retrieveUser() async {
    Response response = await ApiHelper.get('/api/user');
    if (response.statusCode == 200 || response.statusCode == 201) {
      _user = User.fromJson(jsonDecode(response.body));
    } else {
      handleError('Failed to retrieve user based on token.');
    }
    if (_user != null) await _user!.getDevices();
    finishedAuthentication();
  }

  /// Logout and remove token from local storage
  Future<void> logout() async {
    Response response = await ApiHelper.post('/api/auth/logout');
    if (response.statusCode == 200 || response.statusCode == 201) {
      _user = null;
      new FlutterSecureStorage().delete(key: 'token');
      ToastService.showSuccessToast("Successfully signed out.");
      notifyListeners();
    }
  }

  /// Remove token from user;
  void removeToken(Device device) async {
    await _user!.removeDevice(device);
    ToastService.showSuccessToast(
        "Successfully ended session for ${device.name}.");
    notifyListeners();
  }

  /// Get access token from Google
  Future<void> _signInWithGoogle() async {
    accessToken = await new GoogleAuthService().getAccessToken();
    await _signInWithAccessToken('google');
  }

  /// Get access token from social media
  Future<void> signInWithSocialMedia(String socialProvider) async {
    isAuthenticating = true;
    notifyListeners();
    switch (socialProvider) {
      case "Google":
        await _signInWithGoogle();
        break;
      default:
        throw Exception('Sign in with $socialProvider is not (yet) supported.');
    }
  }

  /// Sign in with Email
  Future signInWithEmail(String email, String password) async {

    startAuthentication();

    // Get a user token...
    Response response = await ApiHelper.post('/api/auth/login/email', {
      'email': email,
      'password': password,
      'device_name': await DeviceService.getDeviceName()
    });

    if (response.statusCode == 200 || response.statusCode == 201) {
      String token = jsonDecode(response.body)['token'];
      new FlutterSecureStorage().write(key: 'token', value: token);

      // Retrieve the user...
      await retrieveUser();
    } else {
      handleError('The credentials are invalid.');
    }
  }

}
