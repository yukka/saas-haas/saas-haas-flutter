import 'package:google_sign_in/google_sign_in.dart';

class GoogleAuthService {
  // Define the required scopes of the Google account
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
    ],
  );

  // Retrieve basic Google account details (such as name, email, photo...)
  Future<GoogleSignInAccount?> getGoogleAccount() async {
    GoogleSignInAccount? googleUser = await _googleSignIn.signInSilently();
    return googleUser ?? await _googleSignIn.signIn();
  }

  // Retrieve an access token from an authenticated Google user
  Future<String?> getAccessToken() async {
    try {
      GoogleSignInAccount? googleUser = await getGoogleAccount();
      if (googleUser != null) {
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;
        return googleAuth.accessToken!;
      }
    } catch (error) {
      print(error);
    }
    return null;
  }
}
